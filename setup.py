from setuptools import setup, find_packages
import os

here = os.path.abspath(os.path.dirname(__file__))

README = open(os.path.join(here, 'README.rst')).read()


version = '0.1'


setup(name='pdftableextract2',
    version          = version,
    description      = "Extract tables from PDF files",
    long_description = README + '\n\n',
    classifiers = [
    ],
    keywords     = "PDF, tables",
    author       = "Ian McEwan, Jean-Baptiste Lamy",
    author_email = "jean-baptiste.lamy@univ-paris13.fr",
    license      = "MIT-Expat",
    packages     = ["pdftableextract2"],
    package_dir  = { "pdftableextract2" : "." },
    include_package_data = True,
    zip_safe = False,
    install_requires = ["numpy"],
    entry_points = {
        'console_scripts':
            ['pdftableextract2=pdftableextract2.scripts:main']
    }
)
