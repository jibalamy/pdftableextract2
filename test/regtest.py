import unittest, tempfile, os.path
from io import StringIO

from pdftableextract2.core import process_page, output, o_html


"""
Commands for generating stored results :

python ./pdftableextract2/scripts.py -i ./pdftableextract2/test/doc_test.pdf -p 1 -t html > ./pdftableextract2/test/doc_test.html

python ./pdftableextract2/scripts.py -i ./pdftableextract2/test/aclasta_page_7.pdf -p 1 -t html > ./pdftableextract2/test/aclasta_page_7.html
python ./pdftableextract2/scripts.py -i ./pdftableextract2/test/avonex_page_7.pdf -p 1 -t html > ./pdftableextract2/test/avonex_page_7.html
python ./pdftableextract2/scripts.py -i ./pdftableextract2/test/brilique_page_9.pdf -p 1 -t html > ./pdftableextract2/test/brilique_page_9.html
python ./pdftableextract2/scripts.py -i ./pdftableextract2/test/brilique_page_10.pdf -p 1 -t html > ./pdftableextract2/test/brilique_page_10.html
python ./pdftableextract2/scripts.py -i ./pdftableextract2/test/glivec_page_13.pdf -p 1 -t html > ./pdftableextract2/test/glivec_page_13.html
python ./pdftableextract2/scripts.py -i ./pdftableextract2/test/toviaz_page_7.pdf -p 1 -t html > ./pdftableextract2/test/toviaz_page_7.html
python ./pdftableextract2/scripts.py -i ./pdftableextract2/test/alvesco_image.pdf -p 1 -t html -g 75 --ocr-command 'tesseract -l fra stdin stdout' > ./pdftableextract2/test/alvesco_image.html

"""

class Test(unittest.TestCase):
  def setUp(self):
    self.base_dir = "./pdftableextract2/test/"
    
  def compare_document(self, basename, **kargs):
    results = process_page(os.path.join(self.base_dir, "%s.pdf" % basename), "1", **kargs)
    f = StringIO()
    o_html(results, 1, f)
    html = f.getvalue()
    #open("/tmp/t.html", "w").write(html)
    f = open(os.path.join(self.base_dir, "%s.html" % basename))
    expected = f.read()
    f.close()
    assert html == expected
    
  # LyX / LaTeX document with short but complex tables
  def test_doc_1(self): self.compare_document("doc_test")
    
  # Table with empty space as column separators
  def test_aclasta(self): self.compare_document("aclasta_page_7")
    
  # Table with empty space as line separators
  def test_avonex(self): self.compare_document("avonex_page_7")
    
  # Table with multi-col cells
  def test_brilique_1(self): self.compare_document("brilique_page_9")
  
  # Table with two tables on a single page
  def test_brilique_2(self): self.compare_document("brilique_page_10")
    
  # Table with multi-col cells
  def test_glivec(self): self.compare_document("glivec_page_13")
    
  # Table with multi-col cells
  def test_toviaz(self): self.compare_document("toviaz_page_7")
    
  # Image table with OCR
  def test_alvesco(self): self.compare_document("alvesco_image", greyscale_threshold = 75, ocr_command = 'tesseract -l fra stdin stdout')
    
if __name__ == '__main__':
  unittest.main()
  
  #import cProfile; cProfile.run("unittest.main()")
