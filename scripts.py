import argparse
import sys
import logging
import subprocess

import pdftableextract2.core as core
from pdftableextract2.core import process_page, output

#-----------------------------------------------------------------------

def procargs() :
  p = argparse.ArgumentParser( description="Finds tables in a PDF page.")
  p.add_argument("-i", dest='infile',  help="input file" )
  p.add_argument("-o", dest='outfile', help="output file", default=None,
     type=str)
  p.add_argument("--greyscale_threshold","-g", help="grayscale threshold (%%)", type=int, default=25 )
  p.add_argument("-p", type=str, dest='page', required=True, action="append",
     help="a page in the PDF to process, as page[:firstrow:lastrow]." )
  p.add_argument("-c", type=str, dest='crop',
     help="crop to left:top:right:bottom. Paints white outside this "
          "rectangle."  )
  p.add_argument("--line_length", "-l", type=float, default=0.17 ,
     help="line length threshold (length)" )
  p.add_argument("--bitmap_resolution", "-r", type=int, default=300,
                 help="resolution of internal bitmap (dots per length unit, default: 300)" )
  p.add_argument("-name", help="name to add to XML tag, or HTML comments")
  p.add_argument("-pad", help="imitial image pading (pixels)", type=int,
     default=2 )
  p.add_argument("-white",action="append", 
    help="paint white to the bitmap as left:top:right:bottom in length units."
         "Done before painting black" )
  p.add_argument("-black",action="append", 
    help="paint black to the bitmap as left:top:right:bottom in length units."
         "Done after poainting white" )
  p.add_argument("-bitmap", action="store_true",
     help = "Dump working bitmap not debuging image." )
  p.add_argument("-checkcrop",  action="store_true",
     help = "Stop after finding croping rectangle, and output debuging "
            "image (use -bitmap).")
  p.add_argument("-checklines", action="store_true",
     help = "Stop after finding lines, and output debuging image." )
  p.add_argument("-checkdivs",  action="store_true",
     help = "Stop after finding dividors, and output debuging image." )
  p.add_argument("-checkcells", action="store_true",
     help = "Stop after finding cells, and output debuging image." )
  p.add_argument("-colmult", type=float, default=1.0,
     help = "color cycling multiplyer for checkcells" )
  p.add_argument("-boxes", action="store_true",
     help = "Just output cell corners, don't send cells to pdftotext." )
  p.add_argument("-t", choices=['json','csv','html','list'],
     default="html",
     help = "output format"
            "(default html)" )
  p.add_argument("--whitespace","-w", choices=['none','normalize','raw'], default="normalize",
     help = "What to do with whitespace in cells. none = remove it all, "
            "normalize (default) = any whitespace (including CRLF) replaced "
            "with a single space, raw = do nothing." )
  p.add_argument("--traceback","--backtrace","-tb","-bt",action="store_true")
  p.add_argument("--horizontal-empty-space-threshold", type=int, dest='horizontal_empty_space_threshold', default = 40,
                 help="horizontal empty spaces bigger than threshold are considered as column separator (default 40 pixels, use 0 to disable)." )
  p.add_argument("--vertical-empty-space-threshold", type=int, dest='vertical_empty_space_threshold', default = 40,
                 help="vertical empty spaces bigger than threshold are considered as row separator (default 40 pixels, use 0 to disable)." )
  p.add_argument("--ocr-command", type=str, dest='ocr_command', default = "",
                 help="OCR command; if present, use this command for OCR'ing text (default: no OCR, pdftotext is used; example for the tesseract OCR program: 'tesseract -l fra stdin stdout')." )
  return p.parse_args()

def main():
  try:
    args = procargs()
    imain(args)
  except IOError as e:
    if args.traceback:
        raise
    sys.exit("I/O Error running pdf-table-extract: {0}".format(e))
  except OSError as e:
    print("An OS Error occurred running pdf-table-extract: Is `pdftoppm` installed and available?")
    if args.traceback:
        raise
    sys.exit("OS Error: {0}".format(e))
  except subprocess.CalledProcessError as e:
    if args.traceback:
        raise
    sys.exit("Error while checking a subprocess call: {0}".format(e))

def imain(args):
    cells = []
    if args.checkcrop or args.checklines or args.checkdivs or args.checkcells:
        for page in args.page :
            success = process_page(args.infile, page,
                bitmap=args.bitmap, 
                checkcrop=args.checkcrop, 
                checklines=args.checklines, 
                checkdivs=args.checkdivs,
                checkcells=args.checkcells,
                whitespace=args.whitespace,
                boxes=args.boxes,
                greyscale_threshold=args.greyscale_threshold,
                page=page,
                crop=args.crop,
                line_length=args.line_length,
                bitmap_resolution=args.bitmap_resolution,
                name=args.name,
                pad=args.pad,
                white=args.white,
                black=args.black, outfilename=args.outfile,
                horizontal_empty_space_threshold = args.horizontal_empty_space_threshold,
                vertical_empty_space_threshold   = args.vertical_empty_space_threshold,
                ocr_command = args.ocr_command,
            )

    else:
        for page in args.page :
            
            cells = process_page(args.infile, page,
                bitmap=args.bitmap, 
                checkcrop=args.checkcrop, 
                checklines=args.checklines, 
                checkdivs=args.checkdivs,
                checkcells=args.checkcells,
                whitespace=args.whitespace,
                boxes=args.boxes,
                greyscale_threshold=args.greyscale_threshold,
                page=page,
                crop=args.crop,
                line_length=args.line_length,
                bitmap_resolution=args.bitmap_resolution,
                name=args.name,
                pad=args.pad,
                white=args.white,
                black=args.black,
                horizontal_empty_space_threshold = args.horizontal_empty_space_threshold,
                vertical_empty_space_threshold   = args.vertical_empty_space_threshold,
                ocr_command = args.ocr_command,
            )

            filenames = dict()
            if args.outfile is None:
                args.outfile = sys.stdout
            filenames["{0}_filename".format(args.t)] = args.outfile
            output(cells, page, name=args.name, infile=args.infile, output_type=args.t, **filenames)
            


main()
