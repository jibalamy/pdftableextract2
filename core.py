import sys
import os
from numpy import array, fromstring, ones, zeros, uint8, diff, where, sum, delete
import subprocess
from shlex import quote
from pdftableextract2.pnm import readPNM, writePNM, dumpImage
import re
from xml.dom.minidom import getDOMImplementation
import json
import csv
from io import BytesIO
from collections import defaultdict

#-----------------------------------------------------------------------
def check_for_required_executable(name,command):
  """Checks for an executable called 'name' by running 'command' and supressing
  output. If the return code is non-zero or an OS error occurs, an Exception is raised""" 
  try:
    with open(os.devnull, "w") as fnull:
      result=subprocess.check_call(command,stdout=fnull, stderr=fnull)
  except OSError as e:
    message = """Error running {0}.
Command failed: {1}
{2}""".format(name, " ".join(command), e)
    raise OSError(message)
  except subprocess.CalledProcessError as e:
    raise
  except Exception as e:
    raise

#-----------------------------------------------------------------------
def popen(name, command, *args, **kwargs):
    try:
        result = subprocess.Popen(command, *args, **kwargs)
        return result
    except OSError as e:
        message="""Error running {0}. Is it installed correctly?
Error: {1}""".format(name, e)
        raise OSError(message)
    except Exception as e:
        raise 

def colinterp(a,x) :
    """Interpolates colors"""
    l = len(a)-1
    i = min(l, max(0, int (x * l)))
    (u,v) = a[i:i+2,:]
    return u - (u-v) * ((x * l) % 1.0)

colarr = array([ [255,0,0],[255,255,0],[0,255,0],[0,255,255],[0,0,255] ])

def color(x, colmult=1.0) :
    """colors"""
    return colinterp(colarr,(colmult * x)% 1.0) / 2


class Table(object):
  def __init__(self):
    self.hd = []
    self.vd = []
    self.cells = []
    
  def to_list(self): 
    """Output list of lists"""
    max_col  = max(cell.i for cell in self.cells)
    max_row  = max(cell.j for cell in self.cells)
    
    tab = [ [ "" for i in range(max_col + 1)
            ]    for j in range(max_row + 1)
          ]
    for cell in self.cells: tab[cell.j][cell.i] = cell.text
    return tab
  
  def remove_useless_spans(self):
    rows = defaultdict(list)
    for cell in self.cells:
      rows[cell.j].append(cell)
    for row in rows.values():
      if len({ cell.rowspan for cell in row }) == 1:
        for cell in row: cell.rowspan = 1
        
  def to_html(self):
    html  = """<table border="1">\n"""
    html += """  <tr>\n"""
    current_row = 0
    for cell in self.cells:
      if current_row != cell.j:
        html += """  </tr>\n"""
        html += """  <tr>\n"""
        current_row = cell.j
      attrs = ""
      if cell.colspan != 1: attrs += ' colspan="%s"' % cell.colspan
      if cell.rowspan != 1: attrs += ' rowspan="%s"' % cell.rowspan
      html += """    <td%s>%s</td>\n""" % (attrs, cell.text)
      
    html += """  </tr>\n"""
    html += """</table>\n"""
    return html
  
  def to_json(self): return [cell.to_json() for cell in self.cells]

class Cell(object):
  def __init__(self, table, page, i, j, colspan = 1, rowspan = 1, text = ""):
    self.table = table
    self.page = page
    self.i = i
    self.j = j
    self.colspan = colspan
    self.rowspan = rowspan
    self.text = text
    
  def get_coords(self):
    return (
      self.table.vd[2 *  self.i + 1],
      self.table.vd[2 * (self.i + self.colspan)],
      self.table.hd[2 *  self.j + 1],
      self.table.hd[2 * (self.j + self.rowspan)]
    )

  def to_json(self): return (self.i, self.j, self.colspan, self.rowspan, self.page, self.text)
  
class TextOutsideTable(object):
  def __init__(self, text):
    self.text = text
    
  def to_list(self): return self.text
  
  def to_html(self): return """<div>%s</div>\n""" % self.text
  
  def to_json(self): return self.text
  
def process_page(infile, pgs, 
    outfilename=None,
    greyscale_threshold=25,
    page=None,
    crop=None,
    line_length=0.17,
    bitmap_resolution=300,
    name=None,
    pad=2,
    white=None,
    black=None,
    bitmap=False, 
    checkcrop=False, 
    checklines=False, 
    checkdivs=False,
    checkcells=False,
    whitespace="normalize",
    boxes=False,
    horizontal_empty_space_threshold=40,
    vertical_empty_space_threshold=40,
    ocr_command="",
):
  if checkcrop or checklines or checkdivs or checkcells:
    outfile = open(outfilename, "wb") if outfilename else sys.stdout
  else:
    outfile = open(outfilename, "w") if outfilename else sys.stdout
  page = page or []
  pg, frow, lrow = (list(map(int,(pgs.split(":")))) + [None, None])[0:3]
  #check that pdftoppdm exists by running a simple command
  check_for_required_executable("pdftoppm", ["pdftoppm","-h"])
  #end check
  
  p = popen("pdftoppm", ("pdftoppm -aa no -aaVector no -gray -r %d -f %d -l %d %s " %
      (bitmap_resolution, pg, pg, quote(infile))),
      stdin=subprocess.PIPE, stdout=subprocess.PIPE, shell=True )

#-----------------------------------------------------------------------
# image load secion.

  (maxval, width, height, data) = readPNM(p.stdout)

  pad = int(pad)
  height+=pad*2
  width+=pad*2
  
# reimbed image with a white padd.
  bmp = ones( (height,width) , dtype=bool )

  bmp[pad:height-pad,pad:width-pad] = ( data[:,:] > int(255.0*greyscale_threshold/100.0) )

# Set up Debuging image.
  img = zeros( (height,width,3) , dtype=uint8 )
  img[:,:,0] = bmp*255
  img[:,:,1] = bmp*255
  img[:,:,2] = bmp*255

#-----------------------------------------------------------------------
# Find bounding box.
  t=0
  while t < height and sum(bmp[t,:]==0) == 0 :
    t=t+1
  if t > 0 :
    t=t-1
  
  b=height-1
  while b > t and sum(bmp[b,:]==0) == 0 :
    b=b-1
  if b < height-1:
    b = b+1
  
  l=0
  while l < width and sum(bmp[:,l]==0) == 0 :
    l=l+1
  if l > 0 :
    l=l-1
  
  r=width-1
  while r > l and sum(bmp[:,r]==0) == 0 :
    r=r-1
  if r < width-1 :
    r=r+1

# Mark bounding box.
  #bmp[t,:] = 0
  #bmp[b,:] = 0
  #bmp[:,l] = 0
  #bmp[:,r] = 0

  def boxOfString(x,p) :
    s = x.split(":")
    if len(s) < 4 :
      raise ValueError("boxes have format left:top:right:bottom[:page]")
    return ([bitmap_resolution * float(x) + pad for x in s[0:4] ]
                + [ p if len(s)<5 else int(s[4]) ] ) 


# translate crop to paint white.
  whites = []
  if crop :
    (l,t,r,b,p) = boxOfString(crop,pg) 
    whites.extend( [ (0,0,l,height,p), (0,0,width,t,p),
                     (r,0,width,height,p), (0,b,width,height,p) ] )

# paint white ...
  if white :
    whites.extend( [ boxOfString(b, pg) for b in white ] )

  for (l,t,r,b,p) in whites :
    if p == pg :
      bmp[ t:b+1,l:r+1 ] = 1
      img[ t:b+1,l:r+1 ] = [255,255,255]
  
# paint black ...
  if black :
    for b in black :
      (l,t,r,b) = [bitmap_resolution * float(x) + pad for x in b.split(":") ]
      bmp[ t:b+1,l:r+1 ] = 0
      img[ t:b+1,l:r+1 ] = [0,0,0]

  if checkcrop:
    dumpImage(outfile, bmp, img, bitmap, pad)
    return True
    
#-----------------------------------------------------------------------
# White horizontal line finding section.
#

  white_lines = [bmp[y, l:r].all() for y in range(0, b)]

#-----------------------------------------------------------------------
# Line finding section.
#
# Find all vertical or horizontal lines that are more than rlthresh 
# long, these are considered lines on the table grid.

  lthresh = int(line_length * bitmap_resolution)
  maxdiv=10
  
  hs = zeros(height, dtype=int)
  for j in range(height) :
    dd = diff( where(bmp[j,:]==1)[0] )
    if len(dd) > 0 :
      h = max(dd)
      if h > lthresh: hs[j] = 1
    else:
      # it was a solid black line.
      if bmp[j,0] == 0: hs[j] = 1
  hd = where(diff(hs[:] == 1))[0] + 1
  
  # Look for horizontal dividors that are too large.
  j = 0 
  while j < len(hd):
    if hd[j+1]-hd[j] > maxdiv :
      hd = delete(hd,j)
      hd = delete(hd,j)
    else:
      j=j+2
      
  tables = [Table()]
  if len(hd) > 0:
    tables[-1].hd.append(hd[0])
    tables[-1].hd.append(hd[1])
    for i in range(0, len(hd) - 2, 2):
      if sum(white_lines[hd[i + 1] : hd[i + 2]]) > 2:
        # There is at least two white line between those 2 horizontal dividers
        # => there is 2 different tables
        tables.append(Table())
      tables[-1].hd.append(hd[i + 2])
      tables[-1].hd.append(hd[i + 3])
      
  tables = [table for table in tables if len(table.hd) > 2]

  # Search vertical dividers, in a per-table basis
  for table in tables:
    table_height = table.hd[-1] - table.hd[0]
    
    vs = zeros(width, dtype = int)
    for i in range(width):
      #dd = diff(where(bmp[table.hd[0]:table.hd[-1], i])[0])
      
      white_pixels = where(bmp[table.hd[0]:table.hd[-1], i])[0]
      white_pixels = list(white_pixels)
      if white_pixels: # Required to be sure that diff will go up to the end of the line
        if white_pixels[ 0] !=               0: white_pixels.insert(0, 0)
        if white_pixels[-1] < table_height - 1: white_pixels.append(table_height - 1)
      dd = diff(white_pixels)
      
      if len(dd) > 0:
        v = max(dd)
        if v > lthresh : vs[i] = 1
      else:
        # it was a solid black line.
        if bmp[table.hd[0], i] == 0 : vs[i] = 1
        
    vd = ( where(diff(vs[:]))[0] + 1 )

    # Look for vertical dividors that are too large.
    i = 0
    while i < len(vd) :
      if vd[i+1]-vd[i] > maxdiv :
        vd = delete(vd,i)
        vd = delete(vd,i)
      else:
        i = i + 2
        
    table.vd = list(vd)
    
  tables = [table for table in tables if len(table.vd) > 2]
    
    
  if checklines:
    for j in hd:
      img[j,:] = [0, 255, 0] # green
      
    for table in tables:
      for i in table.vd:
        img[max(0, table.hd[0] - 50) : min(height, table.hd[-1] + 50), i] = [255, 0, 0] # red
        
      for y in range(table.hd[0], table.hd[-1]):
        for x in range(table.vd[0], table.vd[-1]): img[y, x][2] = 0 # yellow
        
    dumpImage(outfile,bmp,img)
    return True
  
#-----------------------------------------------------------------------
# divider checking.
#
# at this point vd holds the x coordinate of vertical  and 
# hd holds the y coordinate of horizontal divider tansitions for each 
# vertical and horizontal lines in the table grid.

  def isDiv(axis, left,right,top,bottom) :
    # Add some tolerancy, because in some PDF there is a blank pixel at the beginning of the line
    if   axis == 0:
      top    += 1
      bottom -= 1
    elif axis == 1:
      left   += 1
      right  -= 1
    #if axis == 0:
    #  print "ISDIV", axis, "   ", left,right,top,bottom, "   ", sum( sum(bmp[t:b, l:r], axis=axis)==0 ) >0
    #  print bmp[top:bottom, left:right]
    # if any col or row (in axis) is all zeros ...
    return sum( sum(bmp[top:bottom, left:right], axis = axis) == 0 ) > 0 
  
  # Remove horizontal dividers that are not linked to at least one vertical divider
  # (e.g. underline)
  # /////// No longer needed
  #hd2 = []
  #for j in range(0,len(hd),2):
  #  for i in range(0,len(vd),2):
  #    if i > 0:
  #      (l,r,t,b) = (vd[i-1], vd[i],   hd[j],   hd[j+1])
  #      if isDiv(1, l,r,t,b):
  #          hd2.append(hd[j])
  #          hd2.append(hd[j+1])
  #          break
  #    
  #hd = hd2
  
  for table in tables:
    table_left   = table.vd[ 0]
    table_right  = table.vd[-1]
    table_top    = table.hd[ 0]
    table_bottom = table.hd[-1]

    def is_close_to_existent_divider(x, divs, limit):
      for div in divs:
        if abs(div - x) < limit: return True
      return False
    
    # Search implicit col dividers (by empty space)
    if horizontal_empty_space_threshold:
      previous_line = img[table_top:table_bottom, table_left]
      empty_spaces = []
      empty_space_start = table_left
      for x in range(table_left + 1, table_right):
        line = img[table_top:table_bottom, x]
        if (line == previous_line).all():
          pass
        else:
          if x - empty_space_start > horizontal_empty_space_threshold:
            for v in vd:
              if empty_space_start <= v <= x: break # Divider already exists
            else:
              empty_spaces.append((empty_space_start, x))
          empty_space_start = x
        previous_line = line
        
      for left, right in empty_spaces:
        middle = (left + right) // 2
        left   = middle - 1
        right  = middle + 1
        if is_close_to_existent_divider(middle, table.vd, 2 * horizontal_empty_space_threshold): continue
        bmp[table_top:table_bottom, left:right] = False
        table.vd.append(left)
        table.vd.append(right)
      table.vd.sort()
      
      
    # Search implicit row dividers (by empty space)
    if vertical_empty_space_threshold and len(hd) and len(vd):
      previous_col = img[table_top, table_left:table_right]
      empty_spaces = []
      empty_space_start = table_top
      for y in range(table_top + 1, table_bottom):
        col = img[y, table_left:table_right]
        if (col == previous_col).all():
          pass
        else:
          if y - empty_space_start > vertical_empty_space_threshold:
            for h in hd:
              if empty_space_start <= h <= y: break # Divider already exists
            else:
              empty_spaces.append((empty_space_start, y))
          empty_space_start = y
        previous_col = col
        
      for top, bottom in empty_spaces:
        middle = (top + bottom) // 2
        top    = middle - 1
        bottom = middle + 1
        if is_close_to_existent_divider(middle, table.hd, 2 * vertical_empty_space_threshold): continue
        bmp[top:bottom, table_left:table_right] = False
        table.hd.append(top)
        table.hd.append(bottom)
      table.hd.sort()
      
      
  if checkdivs :
    img = img / 2
    for table in tables:
      for j in range(0,len(table.hd),2):
        for i in range(0,len(table.vd),2):
          
          if i>0 :
            l,r,t,b = table.vd[i-1], table.vd[i],   table.hd[j],   table.hd[j+1]
            img[ t:b, l:r, 1 ] = 192
            if isDiv(1, l,r,t,b) :
              img[ t:b, l:r, 0 ] = 0
              img[ t:b, l:r, 2 ] = 255
              
          if j>0 :
            l,r,t,b = table.vd[i],   table.vd[i+1], table.hd[j-1], table.hd[j]
            img[ t:b, l:r, 1 ] = 128
            if isDiv(0, l,r,t,b) :
              img[ t:b, l:r, 0 ] = 255
              img[ t:b, l:r, 2 ] = 0
              
    dumpImage(outfile,bmp,img)
    return True
  
#-----------------------------------------------------------------------
# Cell finding section.
# This algorithum is width hungry, and always generates rectangular
# boxes.

  for table in tables:
    table.cells =[] 
    touched = zeros((len(table.hd), len(table.vd)), dtype = bool)
    j = 0
    while j * 2 + 2 < len(table.hd):
      i = 0
      while i * 2 + 2 < len(table.vd):
        u = 1
        v = 1
        if not touched[j, i] :
          while 2+(i+u)*2 < len(table.vd) and \
              not isDiv( 0, table.vd[ 2*(i+u)   ], table.vd[ 2*(i+u)+1],
                            table.hd[ 2*(j+v)-1 ], table.hd[ 2*(j+v)  ]):
            u += 1
          bot = False
          while 2+(j+v)*2 < len(table.hd) and not bot:
            bot = False
            for k in range(1, u + 1) :
              bot |= isDiv( 1, table.vd[ 2*(i+k)-1 ], table.vd[ 2*(i+k)],
                               table.hd[ 2*(j+v)   ], table.hd[ 2*(j+v)+1])
            if not bot:
              v += 1
          table.cells.append(Cell(table, pg, i, j, u, v))
          touched[j:j+v, i:i+u] = True
        i += 1
      j += 1

  if checkcells:
    img = img / 2
    for table in tables:
      nc = float(len(table.cells))
      for k in range(len(table.cells)):
        l,r,t,b = table.cells[k].get_coords()
        img[ t:b, l:r ] += color(k / nc)
        
    dumpImage(outfile, bmp, img)
    return True

#-----------------------------------------------------------------------
# fork out to extract text for each cell.

  whitespace_regexp = re.compile( r'\s+')

  def treat_whitespaces(s):
    if whitespace != 'raw' :
      s = whitespace_regexp.sub( "" if whitespace == "none" else " ", s )
      if len(s) > 0:
        s = s[ (1 if s[0]==' ' else 0) : 
                   len(s) - (1 if s[-1]==' ' else 0) ]
    return s
    
  if not ocr_command:
    def extract_text(pg, l,r,t,b): # pdftotext
      p = popen("pdftotext", 
                "pdftotext -r %d -x %d -y %d -W %d -H %d -layout -nopgbrk -f %d -l %d %s -" % (bitmap_resolution, l-pad, t-pad, r-l, b-t, pg, pg, quote(infile)),
                stdout=subprocess.PIPE,
                shell=True )
      ret = p.communicate()[0].decode("utf8")
      return treat_whitespaces(ret)
    
  else:
    ocr_program = ocr_command.split()[0]
    def extract_text(pg, l,r,t,b): # OCR
      #f = open("/tmp/600_%s_%s_%s_%s.pnm" % (l,r,t,b), "wb")
      #writePNM(f, data[t:b, l:r])
      #f.close()
      
      f = BytesIO()
      writePNM(f, data[t:b, l:r])
      
      p = popen(ocr_program, ocr_command,
                stdin  = subprocess.PIPE,
                stdout = subprocess.PIPE,
                stderr = subprocess.PIPE,
                shell  = True)
      ret = p.communicate(f.getvalue())[0].decode("utf8")
      return treat_whitespaces(ret)
    
  if boxes:
    results = tables
  else :
    if not ocr_command:
    #check that pdftotext exists by running a simple command
      check_for_required_executable("pdftotext",["pdftotext","-h"])
      
    results = []
    
    if tables:
      text_before_table = extract_text(pg, 0, width, 0, tables[0].hd[0])
      if text_before_table.strip(): results.append(TextOutsideTable(text_before_table))
      
      for i in range(len(tables)):
        table = tables[i]
        for cell in table.cells:
          if (frow is None) or (frow <= cell.j <= lrow):
            cell.text = extract_text(pg, *cell.get_coords())
        results.append(table)
      
        if i < len(tables) - 1:
          text_between_tables = extract_text(pg, 0, width, table.hd[-1], tables[i + 1].hd[0])
          if text_between_tables.strip(): results.append(TextOutsideTable(text_between_tables))
          
      text_after_table = extract_text(pg, 0, width, tables[-1].hd[-1], height)
      if text_after_table.strip(): results.append(TextOutsideTable(text_after_table))
      
    else:
      text = extract_text(pg, 0, width, 0, height)
      if text.strip(): results.append(TextOutsideTable(text))

  for table in tables: table.remove_useless_spans()
    
  return results

#-----------------------------------------------------------------------
#output section.

def output(cells, pgs, 
                json_filename=None, 
                csv_filename =None,
                html_filename=None,
                list_filename=None,
                infile=None, name=None, output_type=None
                ):
                
    output_types = [
             dict(filename=json_filename, function=o_json), 
             dict(filename=csv_filename , function=o_csv),
             dict(filename=html_filename, function=o_html),
             dict(filename=list_filename, function=o_list)
             ]
             
    for entry in output_types:
        if entry["filename"]:
            if entry["filename"] != sys.stdout:
                outfile = open(entry["filename"], 'w')
            else:
                outfile = sys.stdout
            
            entry["function"](cells, pgs, 
                                outfile=outfile, 
                                name=name, 
                                infile=infile, 
                                output_type=output_type)

            if entry["filename"] != sys.stdout:
                outfile.close()
        
def o_json(results, pgs, outfile=None, infile=None, name=None, output_type=None) :
  """Output JSON formatted cell data"""
  outfile = outfile or sys.stdout
  #defaults
  infile=infile or ""
  name=name or ""
  
  json_results = [result.to_json() for result in results]
  
  json.dump({ 
    "src": infile,
    "name": name,
    "number_of_table": len([result for result in results if isinstance(result, Table)]),
    "cell_colnames": ( "x","y","width","height","page","contents" ),
    "elements": json_results
    }, outfile)
  outfile.write("\n")

def results_to_list(results) : 
  """Output list of lists"""
  tab = []
  for result in results: tab.append(result.to_list())
  return tab

def o_csv(results, pgs, outfile=None, name=None, infile=None, output_type=None) :
  """Output CSV formatted table"""
  outfile = outfile or sys.stdout
  tab     = results_to_list(results)
  for t in tab:
    if isinstance(t, list): # Else, it is a text outside tables
      csv.writer(outfile).writerows(t)
      
      
def o_list(results, pgs, outfile=None, name=None, infile=None, output_type=None) :
  """Output list of lists"""
  outfile = outfile or sys.stdout
  tab = results_to_list(results)
  print(tab)
  

def o_html(results, pgs, outfile=None, output_type=None, name=None, infile=None) : 
  """Output HTML formatted table"""
  html  = """<?xml version="1.0" encoding="utf-8"?>
<html>
<head><meta charset="utf-8"/></head>
<body>
"""
  for result in results:
    html += result.to_html()
    
  html += """</body>
</html>
"""
  outfile.write(html)
  
